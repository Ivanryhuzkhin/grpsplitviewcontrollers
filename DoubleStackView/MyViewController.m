//
//  MyViewController.m
//  DoubleStackView
//
//  Created by Иван Рыжухин on 01/06/2019.
//  Copyright © 2019 Иван Рыжухин. All rights reserved.
//

#import "MyViewController.h"

@interface MyViewController ()

@end

@implementation MyViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [self reloadInputViews];
}

- (void) addConstraintsToViewController: (UIViewController *) container content: (UIViewController *) content constantLeft:(CGFloat) constantLeft constantRight:(CGFloat) constantRight constantTop:(CGFloat) constantTop constantBottom:(CGFloat) constantBottom  {
    NSLayoutConstraint * leftConstraint = [NSLayoutConstraint
                                          constraintWithItem: content.view
                                          attribute:NSLayoutAttributeLeft
                                          relatedBy:NSLayoutRelationEqual
                                          toItem: container.view
                                          attribute:NSLayoutAttributeLeft
                                          multiplier:1.0
                                          constant:constantLeft];
    
    NSLayoutConstraint * topConstraint = [NSLayoutConstraint
                                         constraintWithItem: content.view
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:container.view
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1.0
                                         constant:constantTop];
    
    NSLayoutConstraint * bottomConstraint = [NSLayoutConstraint
                                            constraintWithItem: content.view
                                            attribute:NSLayoutAttributeBottom
                                            relatedBy:NSLayoutRelationEqual
                                            toItem:container.view
                                            attribute:NSLayoutAttributeBottom
                                            multiplier:1.0
                                            constant:constantBottom];
    
    NSLayoutConstraint * rightConstraint = [NSLayoutConstraint
                                             constraintWithItem: content.view
                                             attribute:NSLayoutAttributeRight
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:container.view
                                             attribute:NSLayoutAttributeRight
                                             multiplier:1.0
                                             constant:constantRight];
    

    
    [container.view addConstraints: @[leftConstraint, topConstraint, rightConstraint, bottomConstraint]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
