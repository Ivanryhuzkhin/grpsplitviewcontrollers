//
//  AppDelegate.m
//  DoubleStackView
//
//  Created by Иван Рыжухин on 30/05/2019.
//  Copyright © 2019 Иван Рыжухин. All rights reserved.
//

#import "AppDelegate.h"
#import "MyViewController.h"
#import "MyTabBarController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
//    UIDevice *device = [UIDevice currentDevice];                    //Get the device object
//    [device beginGeneratingDeviceOrientationNotifications];
//    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];    //Get the notification centre for the app
//    [nc addObserver:self                                            //Add yourself as an observer
//           selector:@selector(orientationChanged:)
//               name:UIDeviceOrientationDidChangeNotification
//             object:device];
    
    MyTabBarController *tabBarController = [MyTabBarController alloc];
    
    
    MyViewController *myViewControllerContainerVertical = [MyViewController alloc];
    myViewControllerContainerVertical.tabBarItem.title = @"Vert";
    MyViewController *myViewControllerLeft = [MyViewController alloc];
    myViewControllerLeft.view.backgroundColor = UIColor.redColor;
    MyViewController *myViewControllerRight = [MyViewController alloc];
    myViewControllerRight.view.backgroundColor = UIColor.blueColor;
    
    MyViewController *myViewControllerContainerHorizontal = [MyViewController alloc];
    myViewControllerContainerHorizontal.tabBarItem.title = @"Horizontal";
    MyViewController *myViewControllerTop = [MyViewController alloc];
    myViewControllerTop.view.backgroundColor = UIColor.greenColor;
    MyViewController *myViewControllerBottom = [MyViewController alloc];
    myViewControllerBottom.view.backgroundColor = UIColor.yellowColor;
    
    CGFloat screenHeight = UIScreen.mainScreen.bounds.size.height;
    CGFloat screenWidth = UIScreen.mainScreen.bounds.size.width;
    
    
    myViewControllerLeft.view.frame = CGRectZero;
    myViewControllerRight.view.frame = CGRectZero;
    
    myViewControllerLeft.view.translatesAutoresizingMaskIntoConstraints = NO;
    myViewControllerRight.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    myViewControllerTop.view.frame = CGRectZero;
    myViewControllerBottom.view.frame = CGRectZero;
    
    myViewControllerTop.view.translatesAutoresizingMaskIntoConstraints = NO;
    myViewControllerBottom.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    
    [self addControllerToContentController:myViewControllerContainerVertical content:myViewControllerLeft];
    [self addControllerToContentController:myViewControllerContainerVertical content:myViewControllerRight];
    
    [self addControllerToContentController:myViewControllerContainerHorizontal content:myViewControllerTop];
    [self addControllerToContentController:myViewControllerContainerHorizontal content:myViewControllerBottom];
    
    [myViewControllerContainerVertical addConstraintsToViewController: myViewControllerContainerVertical content:myViewControllerLeft constantLeft: 0 constantRight: -(screenWidth / 2) constantTop:0 constantBottom:0];
    [myViewControllerContainerVertical addConstraintsToViewController: myViewControllerContainerVertical content:myViewControllerRight constantLeft: screenWidth/2 constantRight: 0  constantTop:0 constantBottom:0];
    
    [myViewControllerContainerHorizontal addConstraintsToViewController: myViewControllerContainerHorizontal content:myViewControllerTop constantLeft: 0 constantRight: 0  constantTop:0 constantBottom: -(screenHeight / 2)];
    [myViewControllerContainerHorizontal addConstraintsToViewController: myViewControllerContainerHorizontal content:myViewControllerBottom constantLeft: 0 constantRight: 0  constantTop:(screenHeight / 2) constantBottom:0];
    
    
//    myViewControllerContainerVertical.view.backgroundColor = UIColor.greenColor;
    tabBarController.viewControllers = @[myViewControllerContainerHorizontal,myViewControllerContainerVertical];
    self.window = [[UIWindow alloc] initWithFrame: UIScreen.mainScreen.bounds];
    self.window.rootViewController = tabBarController;
    [self.window makeKeyAndVisible];
    return YES;
}

//- (void)orientationChanged:(NSNotification *)note
//{
//    NSLog(@"Orientation  has changed: %d", [[note object] orientation]);
//
//}


- (void) addControllerToContentController: (UIViewController*)container content:(UIViewController*) content {
    [container addChildViewController:content];
    
    [container.view addSubview:content.view];
    [content didMoveToParentViewController:container];
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
