//
//  main.m
//  DoubleStackView
//
//  Created by Иван Рыжухин on 30/05/2019.
//  Copyright © 2019 Иван Рыжухин. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
