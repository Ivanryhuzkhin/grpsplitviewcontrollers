//
//  MyTabBarController.h
//  DoubleStackView
//
//  Created by Иван Рыжухин on 09/07/2019.
//  Copyright © 2019 Иван Рыжухин. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyTabBarController : UITabBarController

@end

NS_ASSUME_NONNULL_END
