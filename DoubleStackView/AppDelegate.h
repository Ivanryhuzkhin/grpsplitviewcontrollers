//
//  AppDelegate.h
//  DoubleStackView
//
//  Created by Иван Рыжухин on 30/05/2019.
//  Copyright © 2019 Иван Рыжухин. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

