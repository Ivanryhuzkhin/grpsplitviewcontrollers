//
//  MyViewController.h
//  DoubleStackView
//
//  Created by Иван Рыжухин on 01/06/2019.
//  Copyright © 2019 Иван Рыжухин. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyViewController : UIViewController

- (void) addConstraintsToViewController: (UIViewController *) container content: (UIViewController *) content constantLeft:(CGFloat) constantLeft constantRight:(CGFloat) constantRight  constantTop:(CGFloat) constantTop constantBottom:(CGFloat) constantBottom;
@end

NS_ASSUME_NONNULL_END
